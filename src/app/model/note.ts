export class Note {
    id?: string;
    note: String;
    date: Date;
}
