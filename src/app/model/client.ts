import { User } from './user.model';

export class Client extends User {
    code: String;
    dateValidite: String;
    name: String;
    adresses: Adresse[] = [];
    lastName: string;
}
export class Adresse {
    id: string;
    location: string;
    position: {
        longitude: string,
        latitude: string
    };
    default: boolean;
}
