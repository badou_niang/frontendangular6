import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: '', redirectTo: 'dashboard', pathMatch: 'prefix' },
            { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule' },
            { path: 'signup', loadChildren: './signup/signup.module#SignupModule' },
            { path: 'roles', loadChildren: './role/role.module#RoleModule' },
            { path: 'users', loadChildren: './user/user.module#UserModule' },
            { path: 'charts', loadChildren: './charts/charts.module#ChartsModule' },
            { path: 'clients', loadChildren: './client/client.module#ClientModule' },
            { path: 'clients/:id', loadChildren: './client/client.module#ClientModule' },
            { path: 'aides', loadChildren: './aidemenagere/aidemenagere.module#AidemenagereModule' },
            { path: 'missions', loadChildren: './mission/mission.module#MissionModule' },
            { path: 'partenaires', loadChildren: './partenaire/partenaire.module#PartenaireModule' },
            { path: 'forms', loadChildren: './form/form.module#FormModule' },
            { path: 'bs-element', loadChildren: './bs-element/bs-element.module#BsElementModule' },
            { path: 'grid', loadChildren: './grid/grid.module#GridModule' },
            { path: 'components', loadChildren: './bs-component/bs-component.module#BsComponentModule' },
            { path: 'template', loadChildren: './template/template.module#TemplateModule' },
            { path: 'blank-page', loadChildren: './blank-page/blank-page.module#BlankPageModule' }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule {}
