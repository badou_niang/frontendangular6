import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AidemenagereRoutingModule } from './aidemenagere-routing.module';
import { AidemenagereFicheComponent } from './aidemenagere-fiche/aidemenagere-fiche.component';
import { AidemenageresListComponent } from './aidemenageres-list/aidemenageres-list.component';

@NgModule({
  imports: [
    CommonModule,
    AidemenagereRoutingModule
  ],
  declarations: [AidemenagereFicheComponent, AidemenageresListComponent]
})
export class AidemenagereModule { }
