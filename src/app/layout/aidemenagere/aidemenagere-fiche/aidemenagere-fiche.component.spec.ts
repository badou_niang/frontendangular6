import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AidemenagereFicheComponent } from './aidemenagere-fiche.component';

describe('AidemenagereFicheComponent', () => {
  let component: AidemenagereFicheComponent;
  let fixture: ComponentFixture<AidemenagereFicheComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AidemenagereFicheComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AidemenagereFicheComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
