import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AidemenageresListComponent } from './aidemenageres-list.component';

describe('AidemenageresListComponent', () => {
  let component: AidemenageresListComponent;
  let fixture: ComponentFixture<AidemenageresListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AidemenageresListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AidemenageresListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
