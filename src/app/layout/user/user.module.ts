import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserRoutingModule } from './user-routing.module';
import { PageHeaderModule } from './../../shared';
import { UserComponent } from './user.component';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { TemplateModule } from '../template/template.module';
import { NgbModule, NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { UsersEditComponent } from './users-edit/users-edit.component';
import { UsersAddComponent } from './users-add/users-add.component';

@NgModule({
    imports: [CommonModule, PageHeaderModule, UserRoutingModule, 
        FormsModule,TemplateModule,
        HttpModule,NgbModule.forRoot(),NgbModalModule],
    declarations: [UserComponent, UsersEditComponent, UsersAddComponent]
})
export class UserModule { }
