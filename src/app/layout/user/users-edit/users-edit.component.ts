import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { UserService } from '../../../providers/userprovider/user.provider';
import { User } from '../../../model/user.model';
import { routerTransition } from '../../../router.animations';
import { UtilisateurBO } from '../../../model/utilisateurBO';

@Component({
  selector: 'app-users-edit',
  templateUrl: './users-edit.component.html',
  styleUrls: ['./users-edit.component.scss'],
  animations: [routerTransition()]
})
export class UsersEditComponent implements OnInit {

  user: UtilisateurBO = new UtilisateurBO();
  userupdate: UtilisateurBO = new UtilisateurBO();
  constructor(public activeModal: NgbActiveModal, private userprovider: UserService) { }

  ngOnInit() {
    this.userupdate = JSON.parse(JSON.stringify(this.user));
  }

  update(updateForm: NgForm) {
    this.userupdate.username = updateForm.form.value.username;
    this.userupdate.fullname = updateForm.form.value.fullname;
    this.userupdate.email = updateForm.form.value.email;
    this.userupdate.phoneNumber = updateForm.form.value.phoneNumber;
    this.userprovider.update(this.userupdate).subscribe(data => {
      this.user = data;
      this.activeModal.close('Close click');
      alert('mise a jour reussi');
    },
    err => {
      alert(err);
    });
  }

}
