import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClientsListComponent } from './clients-list/clients-list.component';
import { ClientsFicheComponent } from './clients-fiche/clients-fiche.component';
import { ClientsInfoEditComponent } from './clients-fiche/clients-info-edit/clients-info-edit.component';
import { ClientsAdresseAddComponent } from './clients-fiche/clients-adresse-add/clients-adresse-add.component';
import { ClientsAdresseEditComponent } from './clients-fiche/clients-adresse-edit/clients-adresse-edit.component';

const routes: Routes = [
    {
        path: '',
        component: ClientsListComponent
    },
    {
        path: ':id',
        component: ClientsFicheComponent
    },
    {
        path: '',
        component: ClientsInfoEditComponent
    },
    {
        path: '',
        component: ClientsAdresseAddComponent
    },
    {
        path: '',
        component: ClientsAdresseEditComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ClientRoutingModule {}
