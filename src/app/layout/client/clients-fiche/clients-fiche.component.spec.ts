import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientsFicheComponent } from './clients-fiche.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ClientsInfoComponent } from './clients-info/clients-info.component';
import { ClientsAdresseComponent } from './clients-adresse/clients-adresse.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbModalStack } from '@ng-bootstrap/ng-bootstrap/modal/modal-stack';
import { ScrollBar } from '@ng-bootstrap/ng-bootstrap/util/scrollbar';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { UserService } from '../../../providers/userprovider/user.provider';

describe('ClientsFicheComponent', () => {
  let component: ClientsFicheComponent;
  let fixture: ComponentFixture<ClientsFicheComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [BrowserAnimationsModule, RouterTestingModule, HttpClientTestingModule],
      declarations: [ ClientsFicheComponent, ClientsInfoComponent, ClientsAdresseComponent],
      providers: [UserService, NgbModal, NgbModalStack, ScrollBar]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientsFicheComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
