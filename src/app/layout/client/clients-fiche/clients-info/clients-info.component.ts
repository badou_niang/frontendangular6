import { Component, OnInit, Input } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { routerTransition } from '../../../../router.animations';
import { ClientsInfoEditComponent } from '../clients-info-edit/clients-info-edit.component';
import { Client } from '../../../../model/client';

@Component({
  selector: 'app-clients-info',
  templateUrl: './clients-info.component.html',
  styleUrls: ['./clients-info.component.scss'],
  animations: [routerTransition()]
})
export class ClientsInfoComponent implements OnInit {

  @Input('client')client: Client = new Client ();

  constructor(private modalService: NgbModal) {
  }

  ngOnInit() {
  }
  open() {
    const modal: NgbModalRef = this.modalService.open(ClientsInfoEditComponent);
    (<ClientsInfoEditComponent>modal.componentInstance).client = this.client;
  }
}
