import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientsInfoComponent } from './clients-info.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule, NgbModalModule, NgbModal } from '@ng-bootstrap/ng-bootstrap';

describe('ClientsInfoComponent', () => {
  let component: ClientsInfoComponent;
  let fixture: ComponentFixture<ClientsInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[BrowserAnimationsModule,NgbModule.forRoot(),NgbModalModule],
      declarations: [ ClientsInfoComponent ],
      providers:[NgbModal]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientsInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
