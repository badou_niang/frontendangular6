import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientsAdresseComponent } from './clients-adresse.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbModalStack } from '@ng-bootstrap/ng-bootstrap/modal/modal-stack';
import { ScrollBar } from '@ng-bootstrap/ng-bootstrap/util/scrollbar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('ClientsAdresseComponent', () => {
  let component: ClientsAdresseComponent;
  let fixture: ComponentFixture<ClientsAdresseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[BrowserAnimationsModule],
      declarations: [ ClientsAdresseComponent ],
      providers:[NgbModal,NgbModalStack,ScrollBar]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientsAdresseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
