import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { Client, Adresse } from '../../../../model/client';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NgForm } from '@angular/forms';
import { ClientService } from '../../../../providers/clientprovider/client.service';
import { UserService } from '../../../../providers/userprovider/user.provider';
import { User } from '../../../../model/user.model';

@Component({
  selector: 'app-clients-adresse-edit',
  templateUrl: './clients-adresse-edit.component.html',
  styleUrls: ['./clients-adresse-edit.component.scss'],
  animations: [routerTransition()]
})
export class ClientsAdresseEditComponent implements OnInit {

  client: Client = new Client();
  adresse: Adresse = {id: null, location: '', default: false, position: null};
  adresseupdate: Adresse = new Adresse();

  constructor(public activeModal: NgbActiveModal, private userservice: UserService) { }

  ngOnInit() {
    this.adresseupdate.id = this.adresse.id;
    this.adresseupdate.location = this.adresse.location;
    this.adresseupdate.position = this.adresse.position;
    this.adresseupdate.default = this.adresse.default;
  }

  update(updateForm: NgForm) {
    if (this.adresseupdate.default) {
    for (const item of this.client.adresses) {
      if (item.location !== this.adresse.location) {
      item.default = false;
      }
      if (this.adresseupdate.id === item.id) {
        item.id = this.adresseupdate.id;
        item.location = this.adresseupdate.location;
        item.position = this.adresseupdate.position;
        item.default = this.adresseupdate.default;
      }
    }
    }
    this.userservice.update(this.client).subscribe(data => {
      alert('Les informations ont été mis à jour');
      this.activeModal.close();
    },
    err => {
      alert('Impossible de mettre à jour ces informations');
    });
  }

}
