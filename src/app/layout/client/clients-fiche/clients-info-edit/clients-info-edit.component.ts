import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { User } from '../../../../model/user.model';
import { NgForm } from '@angular/forms';
import { UserService } from '../../../../providers/userprovider/user.provider';
import { UtilisateurBO } from '../../../../model/utilisateurBO';
import { Client } from '../../../../model/client';

@Component({
  selector: 'app-clients-info-edit',
  templateUrl: './clients-info-edit.component.html',
  styleUrls: ['./clients-info-edit.component.scss'],
  animations: [routerTransition()]
})
export class ClientsInfoEditComponent implements OnInit {
  client: Client = new Client();
  clientupdate: Client = new Client();

  constructor(public activeModal: NgbActiveModal, private userservice: UserService) {
   }

  ngOnInit() {
    this.clientupdate = JSON.parse(JSON.stringify(this.client));
  }

  update(updateForm: NgForm) {
    this.clientupdate.fullname = updateForm.form.value.fullname;
    this.clientupdate.lastName = updateForm.form.value.lastName;
    this.clientupdate.phoneNumber = updateForm.form.value.phoneNumber;
    this.clientupdate.email = updateForm.form.value.email;
    console.log('Avant envoi');
    console.log(this.clientupdate.lastName);
    this.userservice.update(this.clientupdate).subscribe(data => {
      this.client = data;
      console.log('apres envoi');
      console.log(this.client.lastName);
      alert('Les informations du client ont été mise à jour');
      this.activeModal.close('Close click');
    },
    err => {
      alert(err);
    });
  }

}
