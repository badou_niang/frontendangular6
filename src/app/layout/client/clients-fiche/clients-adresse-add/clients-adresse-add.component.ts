import { Component, OnInit } from '@angular/core';
import { Client, Adresse } from '../../../../model/client';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NgForm } from '@angular/forms';
import { routerTransition } from '../../../../router.animations';
import { ClientService } from '../../../../providers/clientprovider/client.service';
import { UserService } from '../../../../providers/userprovider/user.provider';
import { User } from '../../../../model/user.model';

@Component({
  selector: 'app-clients-adresse-add',
  templateUrl: './clients-adresse-add.component.html',
  styleUrls: ['./clients-adresse-add.component.scss'],
  animations: [routerTransition()]
})
export class ClientsAdresseAddComponent implements OnInit {

  client: Client = new Client();
  adresse: Adresse = new Adresse();
  constructor(public activeModal: NgbActiveModal, private userservice: UserService) {
   }

  ngOnInit() {
  }
  update(updateForm: NgForm) {
    this.adresse = updateForm.form.value;
    this.adresse.id = '' + this.client.adresses.length + 1;
    if (this.adresse.default) {
    for (const item of this.client.adresses) {
      item.default = false;
    }
    }
    this.client.adresses.push(this.adresse);
    this.userservice.update(this.client).subscribe(data => {
      alert('Les informations ont été mis à jour');
      this.activeModal.close();
    }, err => {
      alert('Impossible de mettre à jour les informations');
  });
  }

}
