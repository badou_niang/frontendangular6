import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../../router.animations';

@Component({
  selector: 'app-partenaires-contact2',
  templateUrl: './partenaires-contact2.component.html',
  styleUrls: ['./partenaires-contact2.component.scss'],
  animations: [routerTransition()]
})
export class PartenairesContact2Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
