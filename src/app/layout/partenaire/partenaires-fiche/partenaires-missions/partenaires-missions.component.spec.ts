import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartenairesMissionsComponent } from './partenaires-missions.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('PartenairesMissionsComponent', () => {
  let component: PartenairesMissionsComponent;
  let fixture: ComponentFixture<PartenairesMissionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[BrowserAnimationsModule],
      declarations: [ PartenairesMissionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartenairesMissionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
