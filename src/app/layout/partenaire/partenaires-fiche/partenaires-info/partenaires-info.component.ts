import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { NgbModalRef, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Partenaire } from '../../../../model/partenaire';
import { PartenairesInfoEditComponent } from '../partenaires-info-edit/partenaires-info-edit.component';
import { User } from '../../../../model/user.model';

@Component({
  selector: 'app-partenaires-info',
  templateUrl: './partenaires-info.component.html',
  styleUrls: ['./partenaires-info.component.scss'],
  animations: [routerTransition()]
})
export class PartenairesInfoComponent implements OnInit {

 @Input('partenaire')partenaire: Partenaire = new Partenaire();

  constructor(private modalService: NgbModal) {
  }

  ngOnInit() {
  }

  open() {
    const modal: NgbModalRef = this.modalService.open(PartenairesInfoEditComponent);
    (<PartenairesInfoEditComponent>modal.componentInstance).partenaire = this.partenaire;
  }

}
