import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartenairesInfoEditComponent } from './partenaires-info-edit.component';
import { NgbModal, NgbActiveModal, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UserService } from '../../../../providers/userprovider/user.provider';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('PartenairesInfoEditComponent', () => {
  let component: PartenairesInfoEditComponent;
  let fixture: ComponentFixture<PartenairesInfoEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[
        FormsModule,BrowserAnimationsModule,NgbModule.forRoot(),HttpClientTestingModule],
      declarations: [ PartenairesInfoEditComponent ],
      providers:[UserService,NgbModal,NgbActiveModal]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartenairesInfoEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
