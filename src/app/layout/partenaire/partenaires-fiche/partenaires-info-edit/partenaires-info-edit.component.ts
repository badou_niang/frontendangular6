import { Component, OnInit, Input } from '@angular/core';
import { Partenaire } from '../../../../model/partenaire';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NgForm } from '@angular/forms';
import { routerTransition } from '../../../../router.animations';
import { PartenaireService } from '../../../../providers/partenaireprovider/partenaire.service';
import { User } from '../../../../model/user.model';
import { UserService } from '../../../../providers/userprovider/user.provider';

@Component({
  selector: 'app-partenaires-info-edit',
  templateUrl: './partenaires-info-edit.component.html',
  styleUrls: ['./partenaires-info-edit.component.scss'],
  animations: [routerTransition()]
})
export class PartenairesInfoEditComponent implements OnInit {

  partenaire: Partenaire = new Partenaire();
  partenaireupdate: Partenaire = new Partenaire();
  newville = '';
  newcompetence = '';
  constructor(public activeModal: NgbActiveModal, private userprovider: UserService) { }

  ngOnInit() {
    this.partenaireupdate = JSON.parse(JSON.stringify(this.partenaire));
  }

  update(updateForm: NgForm) {
    this.partenaireupdate.societe = updateForm.form.value.societe;
    this.partenaireupdate.adresse = updateForm.form.value.adresse;
    this.userprovider.update(this.partenaireupdate).subscribe(data => {
      this.partenaire = data;
      this.activeModal.close('Close click');
      alert('mise a jour reussi');
    },
    err => {
      alert(err);
    });
  }
  delete(competence: string) {
    this.partenaireupdate.competences.splice(this.partenaireupdate.competences.findIndex(c => c === competence), 1);
  }
  add() {
    const competence = this.newcompetence;
    if (this.partenaireupdate.competences.findIndex(c => c === competence) < 0 && competence !== '') {
      if (!this.partenaireupdate.competences) {this.partenaireupdate.competences = []; }
    this.partenaireupdate.competences.push(competence); }
    this.newcompetence = '';
  }


}
