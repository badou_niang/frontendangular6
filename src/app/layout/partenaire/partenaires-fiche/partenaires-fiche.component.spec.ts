import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartenairesFicheComponent } from './partenaires-fiche.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PartenairesNoteComponent } from './partenaires-note/partenaires-note.component';
import { PartenairesContact1Component } from './partenaires-contact1/partenaires-contact1.component';
import { PartenairesContact2Component } from './partenaires-contact2/partenaires-contact2.component';
import { PartenairesInfoComponent } from './partenaires-info/partenaires-info.component';
import { PartenairesAssurancesComponent } from './partenaires-assurances/partenaires-assurances.component';
import { PartenairesAidesMenagereComponent } from './partenaires-aides-menagere/partenaires-aides-menagere.component';
import { PartenairesMissionsComponent } from './partenaires-missions/partenaires-missions.component';
import { RouterTestingModule } from '@angular/router/testing';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbModalStack } from '@ng-bootstrap/ng-bootstrap/modal/modal-stack';
import { ScrollBar } from '@ng-bootstrap/ng-bootstrap/util/scrollbar';
import { UserService } from '../../../providers/userprovider/user.provider';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('PartenairesFicheComponent', () => {
  let component: PartenairesFicheComponent;
  let fixture: ComponentFixture<PartenairesFicheComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[BrowserAnimationsModule,RouterTestingModule,HttpClientTestingModule],
      declarations: [ PartenairesFicheComponent,PartenairesNoteComponent,PartenairesInfoComponent,PartenairesContact1Component,PartenairesContact2Component,PartenairesAssurancesComponent,PartenairesAidesMenagereComponent,PartenairesMissionsComponent ],
      providers:[UserService,NgbModal,NgbModalStack,ScrollBar]    
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartenairesFicheComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
