import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartenairesListComponent } from './partenaires-list.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TableHeaderComponent } from '../../template/tables/table-header';
import { RouterTestingModule } from '@angular/router/testing';
import { UserService } from '../../../providers/userprovider/user.provider';

describe('PartenairesListComponent', () => {
  let component: PartenairesListComponent;
  let fixture: ComponentFixture<PartenairesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[BrowserAnimationsModule,HttpClientTestingModule,RouterTestingModule],
      declarations: [ PartenairesListComponent,TableHeaderComponent ],
      providers:[UserService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartenairesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
