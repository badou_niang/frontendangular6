import { Component, OnInit, Input } from '@angular/core';
import { Mission } from '../../../../model/mission';
import { routerTransition } from '../../../../router.animations';
import { MissionsClientSendSmsComponent } from '../missions-client-send-sms/missions-client-send-sms.component';
import { NgbModalRef, NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Client } from '../../../../model/client';
import { ClientsFicheComponent } from '../../../client/clients-fiche/clients-fiche.component';
import { ClientsInfoComponent } from '../../../client/clients-fiche/clients-info/clients-info.component';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-missions-client-info',
  templateUrl: './missions-client-info.component.html',
  styleUrls: ['./missions-client-info.component.scss'],
  animations: [routerTransition()]
})
export class MissionsClientInfoComponent implements OnInit {
   @Input('mission')mission: Mission = new Mission();
  // @Input('client') client: Client = new Client();

  constructor(private modalService: NgbModal , private route: ActivatedRoute) { }

  ngOnInit() {
  }

  sendSms() {
    const modal: NgbModalRef = this.modalService.open(MissionsClientSendSmsComponent);
    (<MissionsClientSendSmsComponent>modal.componentInstance).mission = this.mission;
  }

  openfiche() {}

}
