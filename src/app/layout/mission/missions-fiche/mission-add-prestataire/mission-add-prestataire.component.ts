import { Component, OnInit } from '@angular/core';
import { Mission } from '../../../../model/mission';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { MissionService } from '../../../../providers/missionprovider/missionprovider';
import { NgForm } from '@angular/forms';
import { routerTransition } from '../../../../router.animations';

@Component({
  selector: 'app-mission-add-prestataire',
  templateUrl: './mission-add-prestataire.component.html',
  styleUrls: ['./mission-add-prestataire.component.scss'],
  animations: [routerTransition()]
})
export class MissionAddPrestataireComponent implements OnInit {
  mission: Mission = new Mission();
  fullname = '' ;
  phoneNumber = '';
  societe = '';
  constructor(public activeModal: NgbActiveModal, private missionservice: MissionService) {
  }

  ngOnInit() {
  }
  add(addForm: NgForm) {
    this.missionservice.affect(this.mission).subscribe(data => {
      alert('L\'aide menagere a été bien affecté');
      this.activeModal.close();
    }, err => {
      alert('Impossible de d\'ajouter les informations');
  });
  }
}
