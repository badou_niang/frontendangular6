import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MissionAddPrestataireComponent } from './mission-add-prestataire.component';

describe('MissionAddPrestataireComponent', () => {
  let component: MissionAddPrestataireComponent;
  let fixture: ComponentFixture<MissionAddPrestataireComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MissionAddPrestataireComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MissionAddPrestataireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
