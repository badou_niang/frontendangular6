import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Mission } from '../../../../model/mission';

@Component({
  selector: 'app-mission-add-coupon',
  templateUrl: './mission-add-coupon.component.html',
  styleUrls: ['./mission-add-coupon.component.scss'],
  animations: [routerTransition()]
})
export class MissionAddCouponComponent implements OnInit {
  @Input('mission') mission: Mission = new Mission();

  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit() {
  }

}
