import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MissionsInterventionComponent } from './missions-intervention.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModalModule, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbModalStack } from '@ng-bootstrap/ng-bootstrap/modal/modal-stack';
import { ScrollBar } from '@ng-bootstrap/ng-bootstrap/util/scrollbar';

describe('MissionsInterventionComponent', () => {
  let component: MissionsInterventionComponent;
  let fixture: ComponentFixture<MissionsInterventionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ BrowserAnimationsModule, NgbModalModule ],
      declarations: [ MissionsInterventionComponent ],
      providers: [ NgbModal, NgbModalStack, ScrollBar]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MissionsInterventionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
