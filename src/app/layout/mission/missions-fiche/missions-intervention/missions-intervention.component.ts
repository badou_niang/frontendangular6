import { Component, OnInit, Input } from '@angular/core';
import { Mission } from '../../../../model/mission';
import { routerTransition } from '../../../../router.animations';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute } from '@angular/router';
import { MissionService } from '../../../../providers/missionprovider/missionprovider';
import { MissionEditRdvComponent } from '../mission-edit-rdv/mission-edit-rdv.component';

@Component({
  selector: 'app-missions-intervention',
  templateUrl: './missions-intervention.component.html',
  styleUrls: ['./missions-intervention.component.scss'],
  animations: [routerTransition()]
})
export class MissionsInterventionComponent implements OnInit {
  @Input('mission') mission: Mission = new Mission();
  constructor(public modalService: NgbModal, private route: ActivatedRoute,
  private missionservice: MissionService) { }

  ngOnInit() {
    console.log(this.mission);
  }

  openEdit() {
    const modal: NgbModalRef = this.modalService.open(MissionEditRdvComponent);
    (<MissionEditRdvComponent>modal.componentInstance).mission = this.mission;
  }

}
