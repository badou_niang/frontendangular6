import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MissionsCancelComponent } from './missions-cancel.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModalModule, NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbModalStack } from '@ng-bootstrap/ng-bootstrap/modal/modal-stack';
import { ScrollBar } from '@ng-bootstrap/ng-bootstrap/util/scrollbar';
import { FormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MissionService } from '../../../../providers/missionprovider/missionprovider';
import { RouterTestingModule } from '@angular/router/testing';


describe('MissionsCancelComponent', () => {
  let component: MissionsCancelComponent;
  let fixture: ComponentFixture<MissionsCancelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ BrowserAnimationsModule , NgbModalModule, FormsModule, NgbModalModule, HttpClientTestingModule, RouterTestingModule],
      declarations: [ MissionsCancelComponent ],
      providers: [ NgbModal , NgbActiveModal, NgbModalStack , ScrollBar, MissionService]

    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MissionsCancelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
