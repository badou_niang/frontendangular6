import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MissionsPrestataireComponent } from './missions-prestataire.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModalModule, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbModalStack } from '@ng-bootstrap/ng-bootstrap/modal/modal-stack';
import { ScrollBar } from '@ng-bootstrap/ng-bootstrap/util/scrollbar';

describe('MissionsPrestataireComponent', () => {
  let component: MissionsPrestataireComponent;
  let fixture: ComponentFixture<MissionsPrestataireComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MissionsPrestataireComponent ], imports: [ BrowserAnimationsModule , NgbModalModule]
    , providers: [ NgbModal , NgbModalStack, ScrollBar]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MissionsPrestataireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
