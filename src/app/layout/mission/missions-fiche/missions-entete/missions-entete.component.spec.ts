import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MissionsEnteteComponent } from './missions-entete.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModalModule, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbModalStack } from '@ng-bootstrap/ng-bootstrap/modal/modal-stack';
import { ScrollBar } from '@ng-bootstrap/ng-bootstrap/util/scrollbar';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MissionService } from '../../../../providers/missionprovider/missionprovider';
import { UserService } from '../../../../providers/userprovider/user.provider';

describe('MissionsEnteteComponent', () => {
  let component: MissionsEnteteComponent;
  let fixture: ComponentFixture<MissionsEnteteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ BrowserAnimationsModule, RouterTestingModule, HttpClientTestingModule],
      declarations: [ MissionsEnteteComponent ],
      providers: [ MissionService, NgbModal, NgbModalStack, ScrollBar, UserService , MissionService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MissionsEnteteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
