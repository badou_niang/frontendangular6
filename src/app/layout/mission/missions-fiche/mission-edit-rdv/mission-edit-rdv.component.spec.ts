import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MissionEditRdvComponent } from './mission-edit-rdv.component';

describe('MissionEditRdvComponent', () => {
  let component: MissionEditRdvComponent;
  let fixture: ComponentFixture<MissionEditRdvComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MissionEditRdvComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MissionEditRdvComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
