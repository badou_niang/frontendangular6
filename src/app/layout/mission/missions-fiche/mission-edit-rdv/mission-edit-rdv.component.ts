import { Component, OnInit, Input , ChangeDetectionStrategy} from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { Mission } from '../../../../model/mission';
import { NgbActiveModal, NgbDateNativeAdapter, NgbDateAdapter } from '@ng-bootstrap/ng-bootstrap';
import { MissionService } from '../../../../providers/missionprovider/missionprovider';
import { ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import {NgbTimepickerConfig} from '@ng-bootstrap/ng-bootstrap';
import {NgbTimeStruct} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-mission-edit-rdv',
  templateUrl: './mission-edit-rdv.component.html',
  styleUrls: ['./mission-edit-rdv.component.scss'],
  providers: [{provide: NgbDateAdapter, useClass: NgbDateNativeAdapter}],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [routerTransition()]
})
export class MissionEditRdvComponent implements OnInit {
  // { provide: OwlDateTimeIntl, useClass: DatetimeLocaleCustomService }
  @Input('mission') mission: Mission = new Mission();
  newMission: Mission = new Mission();
  model1: Date;
  public dateTime: Date;



  constructor(public activeModal: NgbActiveModal ,
  private missionService: MissionService, private route: ActivatedRoute) {}
  ngOnInit() {}

  update(updateForm: NgForm) {
    this.newMission.rdvDate = updateForm.form.value.rdvDate;
    console.log('Avant envoi');
    console.log(this.newMission.rdvDate);
    this.missionService.updateRdv(this.mission.id , this.newMission).subscribe(data => {
      this.mission = data as Mission;
      console.log(this.mission.rdvDate);
      alert('Les informations de la  mission ont été mise à jour');
    },
    err => {
      alert(err);
    });
  }



}
