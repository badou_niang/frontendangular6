import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { MissionAddCouponComponent } from '../mission-add-coupon/mission-add-coupon.component';
import { Mission } from '../../../../model/mission';

@Component({
  selector: 'app-missions-discount',
  templateUrl: './missions-discount.component.html',
  styleUrls: ['./missions-discount.component.scss']
})
export class MissionsDiscountComponent implements OnInit {
  @Input('mission') mission: Mission = new Mission();

  constructor(private modalService: NgbModal, private route: ActivatedRoute) { }

  ngOnInit() {
  }

  openCoupon() {
    const modal: NgbModalRef = this.modalService.open(MissionAddCouponComponent);
    (<MissionAddCouponComponent>modal.componentInstance).mission = this.mission;
  }

}
