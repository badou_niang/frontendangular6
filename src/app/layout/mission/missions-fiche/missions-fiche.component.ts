import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { Mission } from '../../../model/mission';
import { UserService } from '../../../providers/userprovider/user.provider';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-missions-fiche',
  templateUrl: './missions-fiche.component.html',
  styleUrls: ['./missions-fiche.component.scss'],
})
export class MissionsFicheComponent implements OnInit {
  mission: Mission = new Mission();

  constructor(private userservice: UserService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe((param) => {
      this.mission.id = param.id;
    });
    this.get();
    // console.log(this.mission);
  }
  get() {
    this.userservice.findMission(this.mission).subscribe(data => {
      this.mission = data as Mission;
      // console.log(this.mission);
    }, err => {
      alert('Erreur inattendu');
    });
  }

}
