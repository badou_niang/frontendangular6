import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MissionsFicheComponent } from './missions-fiche.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModalModule, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbModalStack } from '@ng-bootstrap/ng-bootstrap/modal/modal-stack';
import { ScrollBar } from '@ng-bootstrap/ng-bootstrap/util/scrollbar';
import { MissionsEnteteComponent } from './missions-entete/missions-entete.component';
import { MissionsClientInfoComponent } from './missions-client-info/missions-client-info.component';
import { MissionsCommandeDetailsComponent } from './missions-commande-details/missions-commande-details.component';
import { MissionsInterventionComponent } from './missions-intervention/missions-intervention.component';
import { MissionsPrestataireComponent } from './missions-prestataire/missions-prestataire.component';
import { MissionsTarificationComponent } from './missions-tarification/missions-tarification.component';
import { MissionsDiscountComponent } from './missions-discount/missions-discount.component';
import { MissionsPaiementComponent } from './missions-paiement/missions-paiement.component';
import { MissionsFacturationComponent } from './missions-facturation/missions-facturation.component';
import { MissionsEventsComponent } from './missions-events/missions-events.component';
import { MissionService } from '../../../providers/missionprovider/missionprovider';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule } from '@angular/forms';
import { UserService } from '../../../providers/userprovider/user.provider';
import { HttpClient } from 'selenium-webdriver/http';
import { HttpClientModule } from '@angular/common/http';

describe('MissionsFicheComponent', () => {
  let component: MissionsFicheComponent;
  let fixture: ComponentFixture<MissionsFicheComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ BrowserAnimationsModule , RouterTestingModule, FormsModule, HttpClientModule],
      declarations: [ MissionsFicheComponent, MissionsEnteteComponent, MissionsClientInfoComponent, MissionsCommandeDetailsComponent,
      MissionsInterventionComponent, MissionsPrestataireComponent, MissionsTarificationComponent, MissionsDiscountComponent,
      MissionsPaiementComponent, MissionsFacturationComponent, MissionsEventsComponent ],
      providers: [ NgbModal , NgbModalStack, ScrollBar, MissionService , UserService]

    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MissionsFicheComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
