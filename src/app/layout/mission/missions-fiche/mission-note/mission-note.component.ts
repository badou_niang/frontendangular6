import { Component, OnInit, Input } from '@angular/core';
import { Mission } from '../../../../model/mission';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { routerTransition } from '../../../../router.animations';
import { NgForm } from '@angular/forms';
import { MissionService } from '../../../../providers/missionprovider/missionprovider';
import { Note } from '../../../../model/Note';

@Component({
  selector: 'app-mission-note',
  templateUrl: './mission-note.component.html',
  styleUrls: ['./mission-note.component.scss'],
  animations: [routerTransition()]
})
export class MissionNoteComponent implements OnInit {

  @Input('mission')mission: Mission = new Mission ();
  missionote: Mission = new Mission ();
  note: Note = new Note ();


  constructor(public activeModal: NgbActiveModal, private missionService: MissionService) { }

  ngOnInit() {
  }

  add(addForm: NgForm) {
    this.missionote.commentaire = addForm.form.value.note;
    this.missionService.addNote(this.mission.id , this.missionote.commentaire).subscribe(data => {
    this.missionote = data as Mission;
    console.log(this.missionService);
    alert('Les informations de la  mission ont été mise à jour');
    this.activeModal.close('Close click');
    },
    err => {
      alert(err);
    });
  }

}
