import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MissionAddEvaluationComponent } from './mission-add-evaluation.component';

describe('MissionAddEvaluationComponent', () => {
  let component: MissionAddEvaluationComponent;
  let fixture: ComponentFixture<MissionAddEvaluationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MissionAddEvaluationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MissionAddEvaluationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
