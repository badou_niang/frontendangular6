import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MissionRoutingModule } from './mission-routing.module';
import { MissionsFicheComponent } from './missions-fiche/missions-fiche.component';
import { MissionsEnteteComponent } from './missions-fiche/missions-entete/missions-entete.component';
import { NgbModule, NgbModalModule, NgbDatepickerModule, NgbModal, NgbTimepickerModule } from '@ng-bootstrap/ng-bootstrap';
import { TemplateModule } from '../template/template.module';
import { MissionsClientInfoComponent } from './missions-fiche/missions-client-info/missions-client-info.component';
import { MissionsCancelComponent } from './missions-fiche/missions-cancel/missions-cancel.component';
import { MissionsFinishComponent } from './missions-fiche/missions-finish/missions-finish.component';
import { MissionsClientSendSmsComponent } from './missions-fiche/missions-client-send-sms/missions-client-send-sms.component';
import { MissionsCommandeDetailsComponent } from './missions-fiche/missions-commande-details/missions-commande-details.component';
import { MissionsInterventionComponent } from './missions-fiche/missions-intervention/missions-intervention.component';
import { MissionsPrestataireComponent } from './missions-fiche/missions-prestataire/missions-prestataire.component';
import { MissionsTarificationComponent } from './missions-fiche/missions-tarification/missions-tarification.component';
import { MissionsDiscountComponent } from './missions-fiche/missions-discount/missions-discount.component';
import { MissionsPaiementComponent } from './missions-fiche/missions-paiement/missions-paiement.component';
import { MissionsFacturationComponent } from './missions-fiche/missions-facturation/missions-facturation.component';
import { MissionsEventsComponent } from './missions-fiche/missions-events/missions-events.component';
import { MissionListComponent } from './mission-list/mission-list.component';
import { MissionService } from '../../providers/missionprovider/missionprovider';
import { FormsModule } from '@angular/forms';
import { MissionNoteComponent } from './missions-fiche/mission-note/mission-note.component';
import { NgbModalStack } from '@ng-bootstrap/ng-bootstrap/modal/modal-stack';
import { MissionAddCouponComponent } from './missions-fiche/mission-add-coupon/mission-add-coupon.component';
import { MissionAddEvaluationComponent } from './missions-fiche/mission-add-evaluation/mission-add-evaluation.component';
import { MissionEditRdvComponent } from './missions-fiche/mission-edit-rdv/mission-edit-rdv.component';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { MissionAddPrestataireComponent } from './missions-fiche/mission-add-prestataire/mission-add-prestataire.component';
@NgModule({
  imports: [
    CommonModule,
    MissionRoutingModule,
    NgbModule.forRoot(),
    TemplateModule,
    NgbModalModule,
    FormsModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
  ],
  declarations: [MissionsFicheComponent, MissionsEnteteComponent, MissionsClientInfoComponent,
     MissionsCancelComponent, MissionsFinishComponent,
     MissionsClientSendSmsComponent, MissionsCommandeDetailsComponent,
     MissionsInterventionComponent, MissionsPrestataireComponent,
     MissionsTarificationComponent, MissionsDiscountComponent,
     MissionsPaiementComponent, MissionsFacturationComponent,
     MissionsEventsComponent, MissionListComponent, MissionNoteComponent,
     MissionAddCouponComponent, MissionAddEvaluationComponent,
     MissionEditRdvComponent,
     MissionAddPrestataireComponent, ],
  providers: [MissionService , NgbModal, NgbModalStack]
})
export class MissionModule { }
