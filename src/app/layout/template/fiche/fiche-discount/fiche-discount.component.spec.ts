import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FicheDiscountComponent } from './fiche-discount.component';

describe('FicheDiscountComponent', () => {
  let component: FicheDiscountComponent;
  let fixture: ComponentFixture<FicheDiscountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FicheDiscountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FicheDiscountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
