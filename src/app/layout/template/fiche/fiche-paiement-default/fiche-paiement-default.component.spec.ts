import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FichePaiementDefaultComponent } from './fiche-paiement-default.component';

describe('FichePaiementDefaultComponent', () => {
  let component: FichePaiementDefaultComponent;
  let fixture: ComponentFixture<FichePaiementDefaultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FichePaiementDefaultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FichePaiementDefaultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
