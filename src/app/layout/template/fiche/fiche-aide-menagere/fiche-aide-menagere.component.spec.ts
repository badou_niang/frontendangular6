import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FicheAideMenagereComponent } from './fiche-aide-menagere.component';

describe('FicheAideMenagereComponent', () => {
  let component: FicheAideMenagereComponent;
  let fixture: ComponentFixture<FicheAideMenagereComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FicheAideMenagereComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FicheAideMenagereComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
