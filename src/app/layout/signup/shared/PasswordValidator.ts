import { FormControl } from '@angular/forms';

export interface ValidationResult {
    [key: string]: boolean;
}

export class PasswordValidator {

    public static strong(control: FormControl): ValidationResult {
        const hasUpper = /[A-Z]/.test(control.value);
        const hasNumber = /\d/.test(control.value);
        const hasLower = /[a-z]/.test(control.value);
        const hasSpecial = /(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{7,}/.test(control.value);
        // console.log('Num, Upp, Low', hasNumber, hasUpper, hasLower);
        const valid = hasUpper ;
        if (!valid) {
            // return what´s not valid
            return { strong: true };
        }
        return null;
    }

    public static lower(control: FormControl): ValidationResult {
        const hasLower = /[a-z]/.test(control.value);
        const valid = hasLower;
        if (!valid) {
            // return what´s not valid
            return { lower: true };
        }
        return null;
    }

    public static number(control: FormControl): ValidationResult {
        const hasNumber = /\d/.test(control.value);
        const valid = hasNumber;
        if (!valid) {
            // return what´s not valid
            return { number: true };
        }
        return null;
    }

    public static special(control: FormControl): ValidationResult {
        const hasSpecial = /(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{0,}/.test(control.value);
        // console.log('Num, Upp, Low', hasNumber, hasUpper, hasLower);
        const valid = hasSpecial;
        if (!valid) {
            // return what´s not valid
            return { special: true };
        }
        return null;
    }

    public static eight(control: FormControl): ValidationResult {
        const hasLength = /[A-Za-z\d$@$!%*?&].{7,}/.test(control.value);
        // console.log('Num, Upp, Low', hasNumber, hasUpper, hasLower);
        const valid = hasLength;
        if (!valid) {
            // return what´s not valid
            return { eight: true };
        }
        return null;
    }
}
