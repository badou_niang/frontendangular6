import { CommonModule } from '@angular/common';
import { HttpClient, HttpClientModule, HttpEvent, HttpInterceptor, HttpHandler,
    HttpRequest, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule, Injectable } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthGuard } from './shared';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RoleService } from './providers/roleprovider/role.service';
import { UserService } from './providers/userprovider/user.provider';
import { CompareValidator } from './layout/signup/shared/compare.validator';
import { CookieService } from 'ngx-cookie-service';
import { SessionService } from './providers/sessionprovider/session.service';
import { AuthService } from './providers/authprovider/auth.provider';
import { ProfileService } from './providers/profileprovider/profile.provider';
import { MissionService } from './providers/missionprovider/missionprovider';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';

/*@Injectable()
export class XhrInterceptor implements HttpInterceptor {

   intercept(req: HttpRequest<any>, next: HttpHandler) {
       const xhr = req.clone({
           headers: req.headers.set('X-Requested-With', 'XMLHttpRequest')
       });
       return next.handle(xhr);
   }
}*/
// AoT requires an exported function for factories
export const createTranslateLoader = (http: HttpClient) => {
    /* for development
    return new TranslateHttpLoader(
        http,
        '/start-angular/SB-Admin-BS4-Angular-6/master/dist/assets/i18n/',
        '.json'
    ); */
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
};

@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: createTranslateLoader,
                deps: [HttpClient]
            }
        }),
        AppRoutingModule,
        FormsModule,
        HttpModule,
        OwlDateTimeModule,
        OwlNativeDateTimeModule,
    ],
    declarations: [AppComponent],
    providers: [AuthGuard, RoleService, UserService , AuthService , ProfileService, CompareValidator, CookieService , SessionService,
    MissionService/*{ provide: HTTP_INTERCEPTORS, useClass: XhrInterceptor, multi: true }*/
    ],
    bootstrap: [AppComponent , ]
})
export class AppModule {}
